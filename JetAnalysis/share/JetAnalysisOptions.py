import AthenaCommon.AtlasUnixStandardJob
from AthenaCommon.AlgSequence import AlgSequence

job = AlgSequence()

from JetAnalysis.JetAnalysisConf import JetAnalysisAlg

job+=JetAnalysisAlg("JetAnalysis")

from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool
ServiceMgr += CfgMgr.THistSvc()

ServiceMgr.EventSelector.InputCollections = ['/eos/user/j/jlieberm/2marcia/AOD.dumpcells.50evts.pool.root']
##['/eos/user/j/jlieberm/2marcia/reco_single_electron_MCe3566_AOD.root']
##['/afs/cern.ch/work/j/jlieberm/public/cells/dumper/files/AOD.dumpcells.50evts.pool.root']
theApp.EvtMax = -1
