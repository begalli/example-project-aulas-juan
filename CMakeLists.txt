cmake_minimum_required( VERSION 3.2 FATAL_ERROR )

if( IS_DIRECTORY ${CMAKE_SOURCE_DIR}/Build/AtlasCMake AND
      NOT AtlasCMake_DIR AND NOT ENV{AtlasCMake_DIR} )
   set( AtlasCMake_DIR ${CMAKE_SOURCE_DIR}/Build/AtlasCMake )
endif()

if( IS_DIRECTORY ${CMAKE_SOURCE_DIR}/Build/AtlasLCG AND
      NOT LCG_DIR AND NOT ENV{LCG_DIR} )
   set( LCG_DIR ${CMAKE_SOURCE_DIR}/Build/AtlasLCG )
endif()


find_package( AtlasCMake QUIET )

find_package( Athena )

atlas_ctest_setup()

set( PROJECT_VERSION Athena ${Athena_VERSION} ) 

atlas_project (JetAnalysis 1.0.0 USE Athena ${Athena_VERSION})


lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
   DESTINATION . )

atlas_cpack_setup()

